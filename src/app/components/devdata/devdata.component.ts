import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginUser } from '../../modules/cdscheck/home/shared/models/login-user';
import { UserRolesService } from '../../modules/cdscheck/home/shared/services/user-roles.service';
import { Input } from '@angular/core/src/metadata/directives';
import { DevdataService } from './devdata.service';
import { Constants} from '../../modules/cdscheck/home/shared/constants/constants';
import { SharedService } from '../../modules/cdscheck/home/shared/services/shared.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-devdata',
  templateUrl: './devdata.component.html'
})
export class DevdataComponent implements OnInit {
  cookieValue;
  cscReqData;
  cscMaintData;
  cscBothData;

  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService,
    private router: Router,
    private route: ActivatedRoute,
    private userRolesService: UserRolesService,
    private devdataService: DevdataService,
    private sharedService: SharedService
  ) { }


  ngOnInit() {
  }

  setRequestCookie() {
    this.devdataService.changeRequestValues(true);
    this.devdataService.displayReplaceValues(false);
    this.devdataService.displayVoidValues(false);
    this.cookieService.set('persona', 'req');
    this.cookieValue = this.cookieService.getAll();
    this.cscReqData = this.httpClient.get(Constants.REST_API_URL+'/user').subscribe(data => {
      this.cscReqData = data;
      this.devdataService.setUser(this.cscReqData.displayName);
      if (this.cscReqData.capabilities == 'CSC_CHECKREQ') {
        this.router.navigate(['home/yourTransactions']);
      }
    }, error => Observable.throw(error),
      () => {
        this.router.navigate(['home/yourTransactions']);
      });
  }

  setMaintCookie() {
    this.devdataService.changeRequestValues(false);
    this.devdataService.displayReplaceValues(true);
    this.devdataService.displayVoidValues(true);
    this.cookieService.set('persona', 'maint');
    this.cookieValue = this.cookieService.getAll();
    this.cscMaintData = this.httpClient.get(Constants.REST_API_URL+'/user').subscribe(data => {
      this.cscMaintData = data;
      this.devdataService.setUser(this.cscMaintData.displayName);
      if (this.cscMaintData.capabilities == 'CSC_CHECKMAINT') {
        this.router.navigate(['home/yourTransactions']);
      }
    }, error => Observable.throw(error),
      () => {
        this.router.navigate(['home/yourTransactions']);
      });
  }

  setCookieForBothUsers() {
    this.devdataService.changeRequestValues(true);
    this.devdataService.displayReplaceValues(true);
    this.devdataService.displayVoidValues(true);
    this.cookieService.set('persona', 'both');
    this.cookieValue = this.cookieService.getAll();
    this.cscBothData = this.httpClient.get(Constants.REST_API_URL+'/user').
      subscribe(data => {
        this.cscBothData = data;
        this.devdataService.setUser(this.cscBothData.displayName);
        if (this.cscBothData.capabilities == 'CSC_CHECKMAINT' && this.cscBothData.capabilities == 'CSC_CHECKREQ') {
          this.router.navigate(['home/yourTransactions']);
        } else {
          // need to add error condition
        }
      }, error => Observable.throw(error),
      () => {
        this.router.navigate(['home/yourTransactions']);
      });
  }

}
