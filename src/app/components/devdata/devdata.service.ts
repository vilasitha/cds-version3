import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DevdataService {

  private requestVar = new BehaviorSubject<boolean>(false);
  private voidVar = new BehaviorSubject<boolean>(false);
  private replaceVar = new BehaviorSubject<boolean>(false);
  private loginUserId = new BehaviorSubject<string>("");

  currentUser = this.loginUserId.asObservable();
  replaceVarMsg = this.replaceVar.asObservable();
  voidVarMsg = this.voidVar.asObservable();
  requestVarMsg = this.requestVar.asObservable();
  constructor() { }

  changeRequestValues(request: boolean) {
    this.requestVar.next(request);
  }

  displayVoidValues(voidCheck: boolean) {
    this.voidVar.next(voidCheck);
  }

  displayReplaceValues(replace: boolean) {
    this.replaceVar.next(replace);
  }
  setUser(userId: string) {
    this.loginUserId.next(userId);
  }


}
