import { Component, OnInit, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../app.service';
import { DevdataComponent } from '../devdata/devdata.component';
import { LoginUser } from '../../modules/cdscheck/home/shared/models/login-user';
import { Observable } from 'rxjs/Observable';
import { DevdataService } from '../devdata/devdata.service';
import { SharedService } from '../../modules/cdscheck/home/shared/services/shared.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  userResults: LoginUser;
  loginUserNameVar: string;
  headerTitle = 'Check Service Center';
  constructor(private devdataService: DevdataService) { }
  ngOnInit() {
    this.getUser();
  }

  /**below method is to get displayName from users api */
  getUser(){
    this.devdataService.currentUser.subscribe( (resp) => this.loginUserNameVar = resp);
  }
  logout() {}


}
