import { Component, OnInit, Input } from '@angular/core';
import { LoginUser } from '../../modules/cdscheck/home/shared/models/login-user';
import { DevdataService } from '../devdata/devdata.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router/src/shared';
import { TransactionsResponse } from '../../modules/cdscheck/home/your-transactions/models/all-trasactions';
import { HomeService } from '../../modules/cdscheck/home/services/home.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls:['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
  // currentTransactiions: TransactionsResponse[] = [];
  // paramClientId: string;
  // checkType: string;
  requestVar: boolean;
  replaceVar: boolean;
  voidVar: boolean;

  constructor(
    private devdataService: DevdataService,
    private activatedroute:ActivatedRoute,
    private homeService:HomeService,
    private router:Router) {
      
     }
  ngOnInit() {
    this.devdataService.requestVarMsg.subscribe((result) => this.requestVar = result);
    this.devdataService.voidVarMsg.subscribe((result) => this.voidVar = result);
    this.devdataService.replaceVarMsg.subscribe((result) => this.replaceVar = result);  
  }

  // loadTransactionsOfId() {
  //   this.currentTransactiions =  this.homeService.getTransactionById(this.paramClientId).subscribe((response) => {
  //      this.currentTransactiions = response
  //      console.log(this.paramClientId);
  //    }
  //    );
  //  }


}
