import { Directive, EventEmitter, Output, ElementRef } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[subTooltip]'
})
export class ToolTipDirective {

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    $(document).ready(function () {
        $('#subTooltip').tooltip({
          position: {
            // my: 'left+5 center+5',
            // at: "right+5 top"
          }
      });
    });
}
}