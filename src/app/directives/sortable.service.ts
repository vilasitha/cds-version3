import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SortService {

    constructor() { }

    private columnSortedSource = new Subject<IColumnSortedEvent>();

    columnSorted$ = this.columnSortedSource.asObservable();
    columnSorted(event: IColumnSortedEvent) {
        this.columnSortedSource.next(event);
    }

}

export interface IColumnSortedEvent {
    sortColumn: string;
    sortDirection: string;
}