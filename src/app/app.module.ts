
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { RouterModule, Routes } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { BreadcrumbModule } from 'angular2-crumbs';
import { appRoutes } from './app.router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorHandler } from '@angular/common/http/src/interceptor';
import { DataTableModule } from "angular2-datatable";
// project related components
import { AppComponent } from './app.component';
import { HomeModule } from './modules/cdscheck/home/home.module';
import { DevdataComponent } from './components/devdata/devdata.component';
import { HomeComponent } from './modules/cdscheck/home/home.component';
import { HeaderComponent } from '../app/components/header/header.component';
import { FooterComponent } from '../app/components/footer/footer.component';
import { SidemenuComponent } from '../app/components/sidemenu/sidemenu.component';
import { PageNotFoundComponent } from '../app/modules/cdscheck/home/page-not-found/page-not-found/page-not-found.component';
// project related services
import { SortService } from './directives/sortable.service';
import { SortableTableDirective } from './directives/sortable-table.directive';
import { AppService } from './app.service';
import { HomeService } from './modules/cdscheck/home/services/home.service';
import { ValidationService } from './modules/cdscheck/home/checktransactions/services/validation.service';
import { CommondropdownService } from './modules/cdscheck/home/checktransactions/services/commondropdown.service';
import { UserRolesService } from './modules/cdscheck/home/shared/services/user-roles.service';
import { DevdataService } from './components/devdata/devdata.service';
import { SharedService } from './modules/cdscheck/home/shared/services/shared.service';


@NgModule({
  declarations: [
    AppComponent,
    DevdataComponent,
    PageNotFoundComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    SidemenuComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    DataTableModule,
    LoggerModule.forRoot({ serverLoggingUrl: '/cdscheck/secure/rest', 
    level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR }),
    RouterModule.forRoot(appRoutes
      // { enableTracing: true }
    ),
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
      HomeService, CommondropdownService, ValidationService,
      SortService,
      AppService,
      UserRolesService, CookieService,
      DevdataService,
      SharedService,
    ],
    exports: [RouterModule],
    bootstrap: [AppComponent]
})
export class AppModule { }
