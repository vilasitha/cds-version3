import { Injectable } from "@angular/core";
import { Component, OnInit } from '@angular/core';
import { RequestOptions, Headers } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";


@Injectable()
export class RestApiService{

    headers: Headers;
    options: RequestOptions;

    constructor(private http: HttpClient) {
        this.headers = new Headers({ 'Content-Type': 'application/json', 
                                     'Accept': 'q=0.8;application/json;q=0.9' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    // deleteServiceWithId(url: string, key: string, val: string): Observable<any> {
    // return this.http
    //     .delete(url + "/?" + key + "=" + val, this.options)
    //     // .map(this.extractData)
    //     .catch(this.handleError);
    // }     

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


}