import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { LoginUser } from '../../shared/models/login-user';
import { OnInit } from '@angular/core';
import { Constants } from '../constants/constants';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';

@Injectable()
export class SharedService {

    constructor() { }

    private checkFlagVar = new BehaviorSubject<boolean>(true);
    currentCheckFlag = this.checkFlagVar.asObservable();
    setCheckFlag(flag: boolean){
        this.checkFlagVar.next(flag);
    }
   

    public transactionsToDisplay = new BehaviorSubject<TransactionsResponse[]>([]);
    currentTransaction = this.transactionsToDisplay.asObservable();

    setTransactionsToDisplay(transactions: TransactionsResponse[]) {
        this.transactionsToDisplay.next(transactions);
    }
}