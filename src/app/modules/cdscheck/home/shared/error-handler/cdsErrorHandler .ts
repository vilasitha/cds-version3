import { ErrorHandler, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';



@Injectable()
export class CdsErrorHandler implements ErrorHandler {

    handleError(error: any) {
        let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        return Observable.throw(error);

    }
}