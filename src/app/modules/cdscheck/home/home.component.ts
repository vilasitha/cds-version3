import { Component, OnInit, Input, HostListener, EventEmitter, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import { Subscription } from 'rxjs/Subscription'
import { HomeService } from './services/home.service';

import { SortService } from '../../../directives/sortable.service';
import { Headers, Response } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../../app.service';
import { LoginUser } from '../home/shared/models/login-user';
import { UserRolesService } from '../home/shared/services/user-roles.service';

@Component({
  selector: '[app-home]',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit() {
  
  }


}
