import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { environment } from '../../../../../environments/environment';
import { TransactionsResponse } from '../your-transactions/models/all-trasactions';
import { Constants } from '../shared/constants/constants';

@Injectable()
export class HomeService {

  constructor(private httpClient: HttpClient) { }
  

  /**
   * display your transactions table
   */
  getCheckTransactions(): any {
    try {   
      return this.httpClient.get<TransactionsResponse>(Constants.REST_API_URL + '/requests').
        catch((error: any) => Observable.throw(error.json().error ||
          'Server Error Occurred while getting Transactions'));
    } catch (error) {
      console.error(error);
    }
  }

  getTransactionById(clientId: string): any {
    try {   
      return this.httpClient.get<TransactionsResponse>('/cdscheck/secure/rest/requests' + '/{requestId}').
        catch((error: any) => Observable.throw(error.json().error ||
          'Server Error Occurred while getting replace check Transactions'));
    } catch (error) {
      console.error(error);
    }
  }


}
