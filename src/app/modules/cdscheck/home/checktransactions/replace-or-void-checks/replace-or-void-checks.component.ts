
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { Location } from '@angular/common';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { validateConfig } from '@angular/router/src/config';
import { Params } from '@angular/router/src/shared';
import { Companies } from '../../shared/models/companies';
import { ConfirmationService } from '../confirmchecks/confirmation.service';
import { HomeService } from '../../services/home.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { HttpParams } from '@angular/common/http/src/params';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-replace-or-void-checks',
  templateUrl: './replace-or-void-checks.component.html',
  styleUrls: ['./replace-or-void-checks.component.css']
})
export class ReplaceOrVoidChecksComponent implements OnInit, AfterViewInit {
  currentTransactions:TransactionsResponse[]=[];
  editReplaceOrVoidChecks: TransactionsResponse[] = [];
  replaceCheckForm: FormGroup;
  companyNames: Companies[] = [];
  checkType: string;
  checkNum: string;
  clientId: string;

  ngAfterViewInit() { }

  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private companyNamesService: CommondropdownService,
    private location: Location,
    private validationService: ValidationService,
    private confirmationService: ConfirmationService,
    private homeService: HomeService,
    private sharedService:SharedService
  ) {
    this.activeRoute.params.subscribe(res => this.clientId = res.clientId);
    this.transactionById(this.clientId)


  }

  ngOnInit() {
      this.activeRoute.queryParams.subscribe((params: Params) => {
       this.checkType = params['type'];
      if (this.checkType === 'Void') {
        this.confirmationService.setVoidChecktMsg(true);
        this.confirmationService.setReplaceCheckMsg(false);
        this.confirmationService.setDisbursementMsg(false);
      } else if (this.checkType === 'Replace') {
        this.confirmationService.setVoidChecktMsg(false);
        this.confirmationService.setReplaceCheckMsg(true);
        this.confirmationService.setDisbursementMsg(false);
      }


    });
    this.displayCompanyNames();
    this.transactionById(this.clientId);
    this.buildForm();
  }

  buildForm(): void {
    this.replaceCheckForm = this.fb.group({
      'companyNames': ['', [this.validationService.companyNamesValidation]],
      'sourceCode': ["813", [Validators.required]],
      'checkNumber': ["", [Validators.pattern('[0-9]+'), this.validationService.checkNumberValidation]],
      'phoneNumber': ["4750"]
    });
  }

  /** display the values in company names dropdown **/
  displayCompanyNames() {
    this.companyNamesService.getCompanyNames().subscribe((res) => {
      this.companyNames = res;
    }, (error: any) => {
      console.log("Error in loading company names table");
    });
  }

  navigateToHome() {
    this.router.navigate(['home/yourTransactions']);
  }

  /**Onsubmit of replce or void checks */
  onSubmit(form: FormGroup): void {
    if (form.valid) {
     if(this.checkType === 'Replace'){
      this.router.navigate(['home/verifyCheckInfo'], {queryParams: { type: 'Replace'}});
      
     }else if(this.checkType === 'Void'){
      this.router.navigate(['home/verifyCheckInfo'], {queryParams: { type: 'Void'}});
      
     }
    } else {
      this.validationService.validateAllFormFields(form);
    }
  } //end

  transactionById(clientId: string) {
    this.homeService.getTransactionById(clientId).
     subscribe((response) => {
        this.currentTransactions = response;
        this.currentTransactions.filter(item => {
          // console.log("Client id in replace checks" +item.clientId);
          item.clientId === this.clientId
        });
      });
  }
}

