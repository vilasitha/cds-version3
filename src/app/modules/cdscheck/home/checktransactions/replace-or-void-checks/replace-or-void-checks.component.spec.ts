import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaceOrVoidChecksComponent } from './replace-or-void-checks.component';

describe('ReplaceOrVoidChecksComponent', () => {
  let component: ReplaceOrVoidChecksComponent;
  let fixture: ComponentFixture<ReplaceOrVoidChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplaceOrVoidChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaceOrVoidChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
