import { TestBed, inject } from '@angular/core/testing';

import { CommondropdownService } from './commondropdown.service';

describe('CommondropdownService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommondropdownService]
    });
  });

  it('should be created', inject([CommondropdownService], (service: CommondropdownService) => {
    expect(service).toBeTruthy();
  }));
});
