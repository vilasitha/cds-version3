import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Constants } from '../../shared/constants/constants';
import { OnInit } from '@angular/core';
import { Companies } from '../../shared/models/companies';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { ReasonCodes } from '../../shared/models/reasons';
import { AltVoidAccounts } from '../../shared/models/voidAccounts';

@Injectable()
export class CommondropdownService implements OnInit {
  private serviceUrl = '/metadata/companies';
  private statesServiceUrl = '/metadata/states';
  private mailcodesServiceUrl = '/metadata/mailCodes';
  private reasonServiceUrl = '/metadata/reasons';
  private voidAccountsServiceUrl = '/metadata/alternateVoidAccounts';
  //need to repalce with backend services
  // private disbursementServiceUrl = '/assets/data/companyName.json';
  private accountTypesServiceUrl = '/assets/data/accountTypes.json';
  // private reasonServiceUrl = '/assets/data/replaceOrVoidReasonData.json';
  // private serviceUrl = 'assets/data/companyName.json';
  // private mailcodesServiceUrl = '/assets/data/mailcodesData.json';

  constructor(private http: Http,
    private httpClient: HttpClient) { }

  ngOnInit() { }

  /**
   * Below method willget the company/bank names.
   */
  getCompanyNames(): any {
    try {
      return this.httpClient.get<Companies>(Constants.REST_API_URL + this.serviceUrl);
    } catch (error) {
      console.error("Error Occrred while calling company name api" + error);
      (error: any) => Observable.throw(error.json().error || 'Server Error While gettinf company names');
    }
  }
  /**
* Below method is to get state codes dropdown.
*/
  public getStatesDropdown(): any {
    try {
      return this.httpClient.get<States>(Constants.REST_API_URL + this.statesServiceUrl);
    } catch (error) {
      console.error("Error Occrred while calling states dropdown" + error);
      (error: any) => Observable.throw(error.json().error || 'Server Error Occurred while getting states drodpown');
    }
  }

  /**
   * Below method is to get mail codes dropdown.
   */
  public getMailcodesDropdown(): any {
    try {
      return this.httpClient.get<MailCodes>(Constants.REST_API_URL + this.mailcodesServiceUrl)
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error while getting Mailcodes drodpown'));
    } catch (error) {
      console.error("Error Occrred while calling MailCodes dropdown" + error);
    }
  }

  /**Below method is to get reason for void or replace code dropdown. */
  public getReasonsDropdown(): any {
    try {
      return this.httpClient.get<ReasonCodes>(Constants.REST_API_URL + this.reasonServiceUrl)
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error while getting reson for replacement or to void drodpown'));
    } catch (error) {
      console.error("Error Occrred while calling Reasons dropdown" + error);
    }
  }

  /**get Void To Acoount number dropdown */
  public getVoidToDropdown(): any {
    try {
      return this.httpClient.get<AltVoidAccounts>(Constants.REST_API_URL + this.voidAccountsServiceUrl)
        .catch((error: any) => Observable.throw(error.json().error || 'Server Error while getting void to drodpown'));
    } catch (error) {
      console.error("Error Occrred while calling Void To dropdown" + error);
    }
  }
  /**
* Below method is to get Account types dropdown.
*/
  public getAccountTypesDD(): Observable<any> {
    return this.http.get(this.accountTypesServiceUrl).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error ||
        'Server Error Occurred while getting Account types drodpown'));
  }

}
