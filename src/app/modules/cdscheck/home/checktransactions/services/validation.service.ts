import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';


export class ValidationService {
  private regex: RegExp = new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g);
  static getValidationErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Field is required.',
      'checkNumRequired': 'Check Number is required',
      'minLength': 'Check Number is minimum 7 characters',
      'maxLength': 'Check Number is maximum 10 characters',
      'companyNamesValidation': 'Company Name/Bank Informantion is a required field.',
      'pattern': 'Check number should be numbers only',
      'date.format.invalid': 'Date should be of format MM/DD/YYYY',
      'companyCodesValidation': 'Company name is required.',
      'sourceCodeValidation': 'Source code is required.',
      'stateCode': 'State is required field.',
      'payeeName': 'Payee is required.',
      'mailCode': 'Mail Code is required field.',
      'payeeAddress': 'Address is required field.',
      'payeeDesc': 'Description is required.',
      'payeeCity':'City is required.',
      'payeeZip': 'Zipcode is required.',
      'payeeZipPlusFour': 'Zipcode plus four is required.',
      'reasonCode':'Need to select a valid reason.',
      'originalCheck':'Please check Yes/No to having Original Check.',
    };
    return config[validatorName];
  }

  public checkNumberValidation(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'checkNumRequired': true };
    }
    else if (control.dirty && control.value.length < 7) {
      return { 'minLength': true };
    }
    else if (control.dirty && control.value.length > 10) {
      return { 'maxLength': true };
    }
  }

  /**Checking company names dropdown value with default text */
  public companyNamesValidation(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '' || control.value.startsWith('Company Name')) {
      return { 'companyNamesValidation': true };
    }
    return null;
  }

  /**Validating Company names for request disbursement */
  public validateCompanyName(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '' || control.value.startsWith('Pick a Company')) {
      return { 'companyCodesValidation': true };
    }
    return null;
  }

  /**Validating source code. */
  public validateSourceCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'sourceCodeValidation': true };
    }
    return null;
  }

  /** Validating state codes. */
  public validateStates(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '' || control.value.startsWith('Select State')) {
      return { 'stateCode': true };
    }
    return null;
  }

  /** Validating state Names. */
  public validateStateNames(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '' || control.value.startsWith('Pick a State')) {
      return { 'stateCode': true };
    }
    return null;
  }

  /**Validating mail codes dropdown. */
  public validateMailCodes(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'mailCode': true };
    }
    return null;
  }
  /**disbursement payee adresss fields validation begin */
  public validatePayeeName(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeName': true };
    }
    return null;
  }
  public validatePayeeAddress(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeAddress': true };
    }
    return null;
  }
  public validatePayeeCity(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeCity': true };
    }
    return null;
  }
  public validatePayeeDescription(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeDesc': true };
    }
    return null;
  }
  public validatePayeeZip(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeZip': true };
    }
    return null;
  }
  public validatepayeeZipPlusFour(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'payeeZipPlusFour': true };
    }
    return null;
  }
  /**End payee adresss fields validation  */

  public validateReasonCode(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '' || control.value.startsWith('Please Select Reason')) {
      return { 'reasonCode': true };
    }
    return null;
  }

  public validateOriginalCheck(control: FormControl): { [key: string]: boolean } | null {
    if (control.value == '') {
      return { 'originalCheck': true };
    }
    return null;
  }
  /**
   * 
   * @param formGroup To validate all the fields in form
   */
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        // console.error("In if of validate");
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
        // console.log("In elseif of validate");
      }
    });
  }

}