import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { concat } from 'rxjs/operator/concat';
import { ReasonCodes } from '../../shared/models/reasons';
import { AltVoidAccounts } from '../../shared/models/voidAccounts';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { SharedService } from '../../shared/services/shared.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';

@Component({
  selector: 'app-verify-check-info',
  templateUrl: './verify-check-info.component.html',
  styleUrls: ['./verify-check-info.component.css']
})
export class VerifyCheckInfoComponent implements OnInit {
  checkType;
  verifyCheckInfoForm: FormGroup;
  reasonCodes: ReasonCodes[]=[];
  payeeState: States[]=[];
  mailCodes: MailCodes[] =[];
  voidTo : AltVoidAccounts[]=[];
  public voidToDate: String;
  enabledOtherReason: boolean = true;
  enabledVoidToSection: boolean = false;
  public isFlag: boolean = true;
  public TisFlag: boolean = true;
  public isDate: boolean = true;
  public reasnVoidLblEnabled :boolean = false;
  public reasnReplaceLblEnabled :boolean = false;
  editTransactions: TransactionsResponse[] = [];
  editClientId;
  data_editPayeeName;
  data_editAdd1;
  data_editAdd2;
  data_editAdd3;
  data_editAdd4;
  data_editCity;
  data_editState;
  data_editZipCode;
  data_editZipPlusFour;
  data_editCompanyName;
  data_editDesc;
  data_editAmount;
  //below fields for edit void check
  data_editVoidTo;
  data_editSSN;
  data_editDate;
  data_editStateCode;
  data_editOriginalCheck
  data_editRFV;
  //below fields for edit replace check
  data_editMailCode;
  data_editDateOfTrans;

  constructor(private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private dropdownService: CommondropdownService,
    private validationService: ValidationService,
  private sharedService: SharedService) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((params: Params) => {
      this.checkType = params['type'];
      if (this.checkType === 'Void') {
        this.reasnVoidLblEnabled = true;
        this.reasnReplaceLblEnabled= false;
      } else if (this.checkType === 'Replace') {
        this.reasnVoidLblEnabled = false;
        this.reasnReplaceLblEnabled= true;
      }
    });
    this.displayReasonCodesDD();
    this.displayMailcodeDD();
    this.displayVoidToDD();
    this.displayStatesDropdown();
    this.editVerifyCheckInfo();
    this.buildVerifyCheckInfoForm();
  }

  buildVerifyCheckInfoForm(): void {
    this.verifyCheckInfoForm = this.fb.group({
      payeeName: [this.data_editPayeeName|| ''],
      amount: [this.data_editAmount || ''],
      verifyDate: [this.data_editDateOfTrans ||''],
      companyCode: [this.data_editCompanyName ||''],
      description: [this.data_editDesc || ''],
      orginalCheckFlag: [this.data_editOriginalCheck || ''],
      reasonCodes: [this.data_editRFV ||'', [this.validationService.validateReasonCode]],
      mailCodes: ['',[this.validationService.validateMailCodes]],
      originalCheck: ['',[this.validationService.validateOriginalCheck]],
      otherReason: [''],
      payeeAddress1: [this.data_editAdd1 ||'', [this.validationService.validatePayeeAddress]],
      payeeAddress2: [this.data_editAdd2|| ''],
      payeeCity: [this.data_editCity || '', [this.validationService.validatePayeeCity]],
      payeeState: [this.data_editState || '', [this.validationService.validateStates]],
      payeeZip: [this.data_editZipCode || '',[this.validationService.validatePayeeZip]],
      payeeZipPlusFour:[this.data_editZipPlusFour ||''],
      voidTo: [''],
      ssn: [''],
      voidToDate: [null, [ref => this.dateValidation(ref)]],
      voidToState: ['']
    });
  }

  //Clicking cancel button will navigate to your transactions page
  navigateToHome() {
    this.router.navigate(['home/yourTransactions']);
  }
  /**Onsubmit of verify check information */
  onSubmit(form: FormGroup): void {
    if (form.valid) {
      if(this.checkType === 'Void')
      this.router.navigate(['home/confirmation'], { queryParams: { type: 'Void' } });
    }else if(this.checkType === 'Replace'){
      this.router.navigate(['home/confirmation'], { queryParams: { type: 'Replace' } });
    } 
    else {
      this.validationService.validateAllFormFields(form);
    }
  } //end

  editVerifyCheckInfo(){
    // this.activeRoute.queryParams.subscribe(params => { this.editClientId = params["clientId"]; });
    // console.log("edit id  ===>" +this.editClientId);
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.editTransactions = response;
        this.editTransactions.forEach(element => {
          if (element.clientId == '13') {
            this.data_editCompanyName = element.companyCode;
            this.data_editDateOfTrans = element.checkDate;
            this.data_editPayeeName = element.payeeName;
            this.data_editMailCode = element.mailCode;
            this.data_editAdd1 = element.payeeAddress1;
            this.data_editAdd2 = element.payeeAddress2;
            this.data_editAdd3 = element.payeeAddress3;
            this.data_editAdd4 = element.payeeAddress4;
            this.data_editCity = element.payeeCity;
            this.data_editState = element.payeeState;
            this.data_editZipCode = element.payeeZip;
            this.data_editZipPlusFour = element.payeeZipPlusFour;
            this.data_editAmount = element.checkAmount;
            this.data_editDesc = element.description1;
            this.data_editRFV = element.reason;
          }
        });
  })}

  /** display the values in reason to void Or replaace check dropdown **/
  displayReasonCodesDD(): any {
    this.dropdownService.getReasonsDropdown().subscribe((res) => {
      this.reasonCodes = res;
    });
  }
  /** display the values in Mail code dropdown **/
  displayMailcodeDD(): any {
    this.dropdownService.getMailcodesDropdown().subscribe((res) => {
      this.mailCodes = res;
    });
  } 
  /** display the values in Void To dropdown **/
  displayVoidToDD(): any {
    this.dropdownService.getVoidToDropdown().subscribe((res) => {
      this.voidTo = res;
    });
  }
   /** display the states dropdown **/
  displayStatesDropdown(): any{
    this.dropdownService.getStatesDropdown().subscribe((res) => {
      this.payeeState= res;
    })
  }
  /**
   * On Change of dropdown value enable Other field.
   */
  handleChange(control: AbstractControl) {
    control = this.verifyCheckInfoForm.get('reasonCodes');
    // console.log("Before If method" + control.value);
    if (control.value == 'Other') {
      this.enabledOtherReason = false;
      // console.log("<====Other Code===>" + control.value);
    } else {
      this.enabledOtherReason = true;
    }
  }
  /**
   * Onchange of void to dorpdown enable ssn, date, state.
   */
  onChangeVoidTo() {
    let control = this.verifyCheckInfoForm.get('voidTo');
    console.log("void to value ====>"+control);
    if (control.value != 'Original Account') {
      this.enabledVoidToSection = true;
    } else {
      this.enabledVoidToSection = false;
    }
  }
  getDate(date) {
    this.voidToDate = date;
    console.log(this.voidToDate);
  }
  /**
   * Date Validation
   */
  public dateValidation(control: AbstractControl): { [key: string]: boolean } | null {
    this.isDate = false;
    if (control.value) {
      if (control.value.length === 2) {
        if (this.isFlag) {
          control.setValue(control.value + '/');
          this.isFlag = false;
        } else {
          control
            .setValue(control
              .value.substring(0, control.value.length - 1));
          this.isFlag = true;
        }
      }
      if (control.value.length === 5) {
        if (this.TisFlag) {
          control.setValue(control.value + '/');
          this.TisFlag = false;
        } else {
          control
            .setValue(control
              .value.substring(0, control.value.length - 1));
          this.TisFlag = true;
        }
      }
      if (this.verifyCheckInfoForm.get('voidToDate').value.length === 10) {
        if ((this.verifyCheckInfoForm.get('voidToDate').value.match(/\//g) || []).length === 2) {
          var arr = this.verifyCheckInfoForm.get('voidToDate').value.split('/');
          var mnth = arr[0] - 1;
          var day = arr[1];
          var year = arr[2];
          var source_date = new Date(year, mnth, day);
          var reGoodDate = /^(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$/;
          if (control.dirty && !reGoodDate.test(control.value) && control.value !== '') {
            if (control.value.length > 8) {
              return { 'date.format.invalid': true };
            }
          }
          if (day != source_date.getDate()) {
            this.isDate = true;
            this.verifyCheckInfoForm.get('voidToDate').setErrors({ 'date.format.invalid': true });
            return { 'date.format.invalid': true };
          } else {
            this.verifyCheckInfoForm.get('voidToDate').setErrors(null);
            return null;

          }
        }
      } else {
        var reGoodDate = /^(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$/;
        if (control.dirty && !reGoodDate.test(control.value) && control.value !== '') {
          if (control.value.length > 8) {
            return { 'date.format.invalid': true };
          } else {
            return { 'date.format.invalid': true };
          }
        } else {
          return null;
        }
      }
    }
  }



}
