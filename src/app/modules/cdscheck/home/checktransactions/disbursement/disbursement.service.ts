import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { RequestDisbursement } from "./models/reqDisbursement";
import { Constants } from "../../shared/constants/constants";
import { Response } from "@angular/http/src/static_response";

@Injectable()
export class DisbursementService {

    disbursementUrl = '/requests'
    constructor(private httpClient: HttpClient) { }


    saveDisbursement(disburseModel: RequestDisbursement) {
        return this.httpClient
            .post<RequestDisbursement>(
            `${Constants.REST_API_URL} + ${this.disbursementUrl}`,
            JSON.stringify(disburseModel),
            { headers: this.getHeaders() }
            ).subscribe(
            (res) => {
                disburseModel = res;
            },
            error => {
                (error: any) => {
                    // console.log('error');
                    if (error.status >= 400 && error.status < 500) {
                        // console.log("400");
                        return Observable.throw('Invalid Data');
                    } else if (error.status === 0) {
                        // console.log("Invalid");
                        return Observable.throw('Invalid URL');
                    }
                    else {
                        // console.log("down");
                        return Observable.throw('Server Down');
                    }
                }
            }
            )

    }

    private getHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return headers;
    }


    // saveDisbursement(model : any[]
    // ){
    //     return this.httpClient.post('https://www.stresources.oneamerica.com/cdscheck/secure/rest/requests',model);
    // }
}