import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { RequestDisbursement } from '../disbursement/models/reqDisbursement';
import { States } from '../../shared/models/states';
import { MailCodes } from '../../shared/models/mailcodes';
import { Companies } from '../../shared/models/companies';
import { SharedService } from '../../shared/services/shared.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';

@Component({
  selector: 'app-disbursement',
  templateUrl: './disbursement.component.html',
  styleUrls: ['./disbursement.component.css']
})
export class DisbursementComponent implements OnInit {

  control: AbstractControl;
  disbursementForm: FormGroup;
  companies: Companies[] = [];
  mailCodes: MailCodes[] = [];
  accountTypes: any;
  states: States[] = [];

  /**Flags to render the fields based on business fucntionality */
  enablePayeeDetails: boolean = false;
  enablePayeeCdYesOrNo: boolean = false;
  enablePayeeCode: boolean = true;
  enableCountry: boolean = false;
  enableState: boolean = true;

  private requestDisbursement: RequestDisbursement;
  private responseDisbursement: RequestDisbursement;

  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private commonDropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.displayCompanyNames();
    this.displayMailCodesDropdown();
    this.displayAccountTypes();
    this.getStates();
    this.enablePayeeDetails;
    this.enablePayeeCdYesOrNo;
    this.enablePayeeCode;
    this.enableCountry;
    this.enableState;
    this.buildForm();
  }

  buildForm() {
    this.disbursementForm = this.fb.group({
      companies: ['0', [this.validationService.validateCompanyName]],
      rbSourceCode: ['', [this.validationService.validateSourceCode]],
      rbPhnNum: ['4750'],
      radioFlag: [''],
      payeeCodeFlag: [''],
      payeeCode: [''],
      descriptionCode: [''],
      states: ['', [this.validationService.validateStateNames]],
      mailCodes: ['4', [this.validationService.validateMailCodes]],
      taxId: [''],
      routingNumber: [''],
      accountNumber: [''],
      accountTypes: [''],
      usOrIntRadioBtn: [''],
      country: ['']

    });
  }


  onCancel() {
    this.router.navigate(['home/yourTransactions']);

  }

  /**Onsubmit of request disbursement */
  onSubmit(form: FormGroup): void {
    if (form.valid) {
      // this.reqDisburseModel.companyCode = this.disbursementForm.get('companyCodes');
      // this.reqDisburseModel.sourceCode =this.disbursementForm.get('rbSourceCode').value; 
      // this.reqDisburseModel.mailCode = this.disbursementForm.get('mailCode').value;
      // this.reqDisburseModel.payeeState = this.disbursementForm.get('stateCode').value;

      // this.reqDisburseModel.bankAccount = this.disbursementForm.get('accountNumber').value ;
      // this.reqDisburseModel.eftFlag = this.disbursementForm.get('radioFlag').value;
      // this.reqDisburseModel.taxId= this.disbursementForm.get('taxId').value;
      // this.reqDisburseModel.phoneNumber = this.disbursementForm.get('phoneNumber').value;
      // this.reqDisburseModel.descriptionCode = this.disbursementForm.get('descriptionCode').value;
      // this.reqDisburseModel.vendorCode = this.disbursementForm.get('payeeCode').value;
      // this.reqDisburseModel.payeeCountry = this.disbursementForm.get('country').value;
      // console.log("In if method");
      this.router.navigate(['home/payeeAddress']);
      // console.log('Request disbursement form submitted' + JSON.stringify(this.reqDisburseModel));
    }
    else {
      this.validationService.validateAllFormFields(form);
    }
  } //endmailCode


  /** get the values in states dropdown **/
  getStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.states = response;
    }),
      (error: any) => {
        console.log("Error in loading states dropdown");
      };
  }

  /** display the values in Mail code dropdown **/
  displayMailCodesDropdown() {
    this.commonDropdownService.getMailcodesDropdown().subscribe((response) => {
      this.mailCodes = response;
    }),
      (error: any) => {
        console.error("Error in loading MailCodes dropdown");
      };
  }
  /** display the values in company names dropdown **/
  displayCompanyNames() {
    this.commonDropdownService.getCompanyNames().subscribe((res) => {
      this.companies = res;
    });
  }
  /** display values in Account types dropdown  */
  displayAccountTypes(): any {
    this.commonDropdownService.getAccountTypesDD().subscribe((resposne) => {
      this.accountTypes = resposne.accountTypes;
    });
  }

  /** render the fields based on money transfer type */
  validateOnChange(control: AbstractControl) {
    let transferType = this.disbursementForm.get('radioFlag');
    if (transferType.value == 'Check') {
      this.enablePayeeCode = true;
      this.enablePayeeCdYesOrNo = false;
      this.enablePayeeDetails = false;
    } else if (transferType.value == 'Electronic Fund Transfer') {
      this.enablePayeeCode = false;
      this.enablePayeeCdYesOrNo = true;
    }
  }//end

  /**  render the fields based on vendor code  */
  validateOnVendorCode(control: AbstractControl) {
    let vendorCode = this.disbursementForm.get('payeeCodeFlag');
    if (vendorCode.value == 'Yes') {
      this.enablePayeeCode = true;
      this.enablePayeeDetails = false;
    } else if (vendorCode.value == 'No') {
      this.enablePayeeCode = false;
      this.enablePayeeDetails = true;
    }
  } //end

  /** render the states or country fields  */
  validateStateOrCountry(control: AbstractControl) {
    let stateOrCountry = this.disbursementForm.get('usOrIntRadioBtn');
    if (stateOrCountry.value === 'U.S') {
      this.enableCountry = false;
      this.enableState = true;
      this.sharedService.setCheckFlag(true);
    } else if (stateOrCountry.value === 'International') {
      this.enableCountry = true;
      this.enableState = false;
      this.sharedService.setCheckFlag(false);
    }
  }//end


}
