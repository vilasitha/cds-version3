import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayeeInfoComponent } from './payee-info.component';

describe('PayeeInfoComponent', () => {
  let component: PayeeInfoComponent;
  let fixture: ComponentFixture<PayeeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayeeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayeeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
