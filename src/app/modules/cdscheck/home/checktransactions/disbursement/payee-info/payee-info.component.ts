import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from './../../services/commondropdown.service';
import { ValidationService } from './../../services/validation.service';
import { SharedService } from '../../../shared/services/shared.service';
import { States } from '../../../shared/models/states';


@Component({
  selector: 'app-payee-info',
  templateUrl: './payee-info.component.html',
  styleUrls: ['./payee-info.component.css']
})
export class PayeeInfoComponent implements OnInit {

  payeeInfoForm: FormGroup;
  showAddressFileds: boolean;
  payeeState: States[] = [];
  constructor(
    private fb: FormBuilder, private router: Router,
    private route: ActivatedRoute,
    private commonDropdownService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.sharedService.currentCheckFlag.subscribe((data) => this.showAddressFileds = data);
    this.getStates();
    this.buildForm();
  }

  buildForm(): void {
    this.payeeInfoForm = this.fb.group({
      payeeName: ['',[this.validationService.validatePayeeName]],
      payeeAddress1: ['', [this.validationService.validatePayeeAddress]],
      payeeAddress2: [''],
      payeeAddress3: [''],
      payeeAddress4: [''],
      payeeCity: ['', [this.validationService.validatePayeeCity]],
      payeeState: ['', [this.validationService.validateStates]],
      payeeZip: ['', [this.validationService.validatePayeeZip]],
      description1: ['', [this.validationService.validatePayeeDescription]],
      description2: [''],
      payeeZipPlusFour: ['',[this.validationService.validatepayeeZipPlusFour]]
    });
  }
  onSubmit(form: FormGroup) {
    if(form.valid){
      this.router.navigate(['home/accoutingDetails']);
    }else{
      this.validationService.validateAllFormFields(form);
    }
    
  }

  onClickPrevious() {
    this.router.navigate(['home/requestDisbursement']);
  }

  onClickCancel() {
    this.router.navigate(['home/yourTransactions']);
  }

  /** get the values in states dropdown **/
  getStates() {
    this.commonDropdownService.getStatesDropdown().subscribe((response) => {
      this.payeeState = response;
    }),
      (error: any) => {
        console.log("Error in loading states dropdown");
      };
  }
}
