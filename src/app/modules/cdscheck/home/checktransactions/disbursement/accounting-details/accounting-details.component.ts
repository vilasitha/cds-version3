import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AccountingDetails } from '../models/account-details';
import { FormGroup, AbstractControl } from '@angular/forms/src/model';
import { FormBuilder } from '@angular/forms';
import { TransactionsResponse } from '../../../your-transactions/models/all-trasactions';
import { ConfirmationService } from '../../confirmchecks/confirmation.service';
import { DisbursementService } from '../disbursement.service';
import { RequestDisbursement } from '../models/reqDisbursement';


@Component({
  selector: 'app-accounting-details',
  templateUrl: './accounting-details.component.html',
  styleUrls: ['./accounting-details.component.css']
})
export class AccountingDetailsComponent implements OnInit {

  accountDetailsForm: FormGroup;
  private newAttribute: any = {};
  @Input()
  private RequestDisbursement= new RequestDisbursement();
  public accountsArray: Array<AccountingDetails> = [];

  constructor(private router: Router,
    private fb: FormBuilder,
    private disbursementService: DisbursementService,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.confirmationService.setDisbursementMsg(true);
    this.buildForm();
  }

  buildForm() {
    this.accountDetailsForm = this.fb.group({
      totalCheckAmount: [''],
      accountNumber: [''],
      amount: [''],
      policyNumber: [''],
      suspenseNumber: [''],
      alpha: [''],
      debitCreditFlag: [''],
      rowId: ['']
    });

  }

  onSave() : void{
    this.disbursementService.saveDisbursement(this.RequestDisbursement);
    // subscribe(
    //   (response) => this.RequestDisbursement = (response) 
    // )
  }



  addFieldValue(details: AccountingDetails) {
    this.accountsArray.push(this.newAttribute);
    this.newAttribute = {};
  }

  private generateId() {
    console.log(Math.round(Math.random() * 1000));
    return Math.round(Math.random() * 1000);
  }
  onSubmit(from: FormGroup) {

    let creditFlag = this.accountDetailsForm.get('debitCreditFlag');
    let debitFlag = this.accountDetailsForm.get('debitCreditFlag');
    let totalAmount = this.accountDetailsForm.get('totalCheckAmount');
    let amount = this.accountDetailsForm.get('amount');

    if (totalAmount != null) {
      this.confirmationService.setDisbursementMsg(true);
      this.confirmationService.setReplaceCheckMsg(false);
      this.confirmationService.setVoidChecktMsg(false);
    } else {
      this.confirmationService.setDisbursementMsg(false);
      this.confirmationService.setReplaceCheckMsg(false);
      this.confirmationService.setVoidChecktMsg(false);     
    }

   
    if (creditFlag.value == 'CR' && amount.value != null) {
      parseInt(totalAmount.value) - parseInt(creditFlag.value);
    }

    this.router.navigate(['home/confirmation']);
  }

  validateAmount(control: AbstractControl) {

  }


  onClickPrevious() {
    this.router.navigate(['home/payeeAddress']);
  }

  onClickCancel() {
    this.router.navigate(['home/yourTransactions']);
  }

}
