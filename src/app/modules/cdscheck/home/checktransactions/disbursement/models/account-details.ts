export class AccountingDetails {

    accountNumber: string;
    amount: string;
    policyNumber: string;
    suspenseNumber: string;
    alpha: string;
    debitCreditFlag: string;

}