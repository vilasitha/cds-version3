import { AccountingDetails } from "./account-details";

export class RequestDisbursement{

    transactionType: string;
    companyCode: string;
    sourceCode: string;
    phoneNumber: string;
    stateCode:string;
    vendorCode: string;
    payeeName: string;
    payeeAddress1: string;
    payeeAddress2: string;
    payeeAddress3: string;
    payeeAddress4: string;
    payeeCity:string;
    payeeState: string;
    payeeZip: string;
    payeeZipPlusFour:string;
    payeeCountry:string;
    payeeAddressIndicator: string;
    taxId: string;
    bankRouting: string;
    bankAccount: string;
    paymentType:string;
    mailCode:string;
    descriptionCode: string;
    description1: string;
    description2: string;
    status:string;
    lastModified:string;
    originalCheckNumber:string;
    originalCheckFlag:string;
    reason:string;
    checkDate:string;
    checkAmount:string;
    voidAccount:string;  
    accountingDetails: Array<AccountingDetails>[];
 
  
}