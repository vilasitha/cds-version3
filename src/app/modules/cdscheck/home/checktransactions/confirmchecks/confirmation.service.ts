import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class ConfirmationService{

    private disbursementVar = new BehaviorSubject<boolean>(false);
    currentDisburseMessage = this.disbursementVar.asObservable();
    setDisbursementMsg(message: boolean){
        this.disbursementVar.next(message);
    }

    private voidCheckVar = new BehaviorSubject<boolean>(false);
    currentVoidMsg = this.voidCheckVar.asObservable();
    setVoidChecktMsg(message: boolean){
        this.voidCheckVar.next(message);
    }
    private replaceCheckVar = new BehaviorSubject<boolean>(false);
    currentReplaceCheckMsg = this.replaceCheckVar.asObservable();
    setReplaceCheckMsg(message: boolean){
        this.replaceCheckVar.next(message);
    }

   
}