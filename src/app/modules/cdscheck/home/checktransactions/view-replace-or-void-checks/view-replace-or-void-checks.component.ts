import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';

import { CommondropdownService } from '../services/commondropdown.service';
import { ValidationService } from '../services/validation.service';
import { TransactionsResponse } from '../../your-transactions/models/all-trasactions';
import { SharedService } from '../../shared/services/shared.service';
import { forEach } from '@angular/router/src/utils/collection';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-view-replace-or-void-checks',
  templateUrl: './view-replace-or-void-checks.component.html',
  styleUrls: ['./view-replace-or-void-checks.component.css'],
  providers: [NGXLogger]
})
export class ViewReplaceOrVoidChecksComponent implements OnInit {
  id: string;
  data_transactionType;
  data_companyName;
  data_bankAccNum;
  data_viewCheckNumber;
  data_viewDateOfTrans;
  data_viewPayee;
  data_viewAdd1;
  data_viewAdd2;
  data_viewAdd3;
  data_viewAdd4;
  data_viewCity;
  data_ViewState;
  data_viewZipCode;
  data_viewSourceCode;
  // data_authorizer;
  data_mailCode;
  data_viewAmount;
  data_viewDesc;
  data_viewRFV;
  data_viewOriginalCheck;

  viewServiceReqForm: FormGroup;
  transactions: TransactionsResponse[] = [];
  constructor(
    private fb: FormBuilder, private router: Router,
    private activeRoute: ActivatedRoute,
    private logger: NGXLogger,
    private companyNamesService: CommondropdownService,
    private validationService: ValidationService,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.getTransactionsInfo();
    this.buildForm();
  }

  /**
   * Building a form with params
   */
  buildForm(): void {
    this.viewServiceReqForm = this.fb.group({
      transactionType: this.data_transactionType || "-- NA --",
      companyName: this.data_companyName || "-- NA --",
      bankName: this.data_bankAccNum || "-- NA --",
      viewCheckNumber: this.data_viewCheckNumber || "-- NA --",
      viewDateOfTrans: this.data_viewDateOfTrans || "-- NA --",
      viewPayee: this.data_viewPayee || "-- NA --",
      viewAdd1: this.data_viewAdd1 || "-- NA --",
      viewAdd2: this.data_viewAdd2 || "-- NA --",
      viewAdd3: this.data_viewAdd3 || "-- NA --",
      viewAdd4: this.data_viewAdd4 || "-- NA --",
      viewCity: this.data_viewCity || "-- NA --",
      ViewState: this.data_ViewState || "-- NA --",
      viewZipCode: this.data_viewZipCode || "-- NA --",
      viewSourceCode: this.data_viewSourceCode || "-- NA --",
      mailCode: this.data_mailCode || "-- NA --",
      viewAmount: this.data_viewAmount || "-- NA --",
      viewDesc: this.data_viewDesc || "-- NA --",
      viewRFV: this.data_viewRFV || "-- NA --",
      viewOriginalCheck: this.data_viewOriginalCheck || "-- NA --"
    });
  }

  getTransactionsInfo() {
    this.activeRoute.queryParams.subscribe(params => { this.id = params["clientId"]; });
    this.sharedService.currentTransaction.subscribe(
      (response) => {
        this.transactions = response;
        this.transactions.forEach(element => {
          if (element.clientId == this.id) {
            this.data_companyName = element.companyCode;
            this.data_transactionType = element.transactionType;
            this.data_bankAccNum = element.bankAccount;
            this.data_viewCheckNumber = element.originalCheckNumber;
            this.data_viewDateOfTrans = element.checkDate;
            this.data_viewPayee = element.payeeName;
            this.data_mailCode = element.mailCode;
            this.data_viewAdd1 = element.payeeAddress1;
            this.data_viewAdd2 = element.payeeAddress2;
            this.data_viewAdd3 = element.payeeAddress3;
            this.data_viewAdd4 = element.payeeAddress4;
            this.data_viewCity = element.payeeCity;
            this.data_ViewState = element.payeeState;
            this.data_viewZipCode = element.payeeZip;
            this.data_viewSourceCode = element.sourceCode;
            this.data_viewAmount = element.checkAmount;
            this.data_viewDesc = element.description1;
            this.data_viewRFV = element.reason;
            this.data_viewOriginalCheck = element.originalCheckNumber;
          }
        });
      }
    )
  }

  //Edit method to navigate to respective pages
  onSubmit(form: FormGroup) {
    this.activeRoute.queryParams.subscribe((params: Params) => {
      let editId = params['clientId'];
      let recordType = this.viewServiceReqForm.get('transactionType');
      if (recordType.value === 'Replacement') {
        this.logger.info("<<==Replacement edit Method==>" + recordType.value);
        this.sharedService.setTransactionsToDisplay(this.transactions);
        this.router.navigate(['home/verifyCheckInfo'], { queryParams: { type: 'Replace' } });
      } else if (recordType.value === 'Void') {
        this.logger.info("<<==Void edit Method==>" + recordType.value);
        this.sharedService.setTransactionsToDisplay(this.transactions);
        this.router.navigate(['home/verifyCheckInfo'], { queryParams: { type: 'Void' } });
      } else if (recordType.value === 'Disbursement') {
        this.logger.info("<<==Disbursement edit Method==>" + recordType.value);
        this.router.navigate(['home/requestDisbursement']);
      }
    });

  }

  onPrint() {
    window.print();
  }

  onClickHome() {
    this.router.navigate(['home/yourTransactions'])
  }
  delete(clientId: any) {
    if (window.confirm("are you sure you want to delete this record (OK=yes Cancel=no)")) {
      // delete method logic here
    }
  }
}
