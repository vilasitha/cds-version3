import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewReplaceOrVoidChecksComponent } from './view-replace-or-void-checks.component';

describe('ViewReplaceOrVoidChecksComponent', () => {
  let component: ViewReplaceOrVoidChecksComponent;
  let fixture: ComponentFixture<ViewReplaceOrVoidChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewReplaceOrVoidChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewReplaceOrVoidChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
