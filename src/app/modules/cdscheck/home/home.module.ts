
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatNativeDateModule } from '@angular/material';
import { MatSortModule }from '@angular/material/sort'
import { RouterModule } from '@angular/router';
import { BreadcrumbModule } from 'angular2-crumbs';
import { homeRoutes } from './home.router';
import { Http, HttpModule, JsonpModule  } from '@angular/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorHandler } from '@angular/common/http/src/interceptor';
import { CookieService } from 'ngx-cookie-service';
import { Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

//Services
import { HomeService} from './services/home.service';
import { ValidationService } from '../home/checktransactions/services/validation.service';
import { CommondropdownService }from '../home/checktransactions/services/commondropdown.service';
import { UserRolesService } from '../home/shared/services/user-roles.service';
import { DevdataService } from '../../../components/devdata/devdata.service';
// Components
import {YourTransactionsComponent} from '../home/your-transactions/your-transactions.component';
import { ControlMessagesComponent } from '../home/checktransactions/error-messages/error-messages-component';
import { DisbursementComponent } from '../home/checktransactions/disbursement/disbursement.component';
import { ReplaceOrVoidChecksComponent  } from '../home/checktransactions/replace-or-void-checks/replace-or-void-checks.component';
import { ViewReplaceOrVoidChecksComponent } from '../home/checktransactions/view-replace-or-void-checks/view-replace-or-void-checks.component';
import { VerifyCheckInfoComponent } from '../home/checktransactions/verify-check-info/verify-check-info.component';
import { ConfirmchecksComponent } from '../home/checktransactions/confirmchecks/confirmchecks.component';
import { PayeeInfoComponent } from '../home/checktransactions/disbursement/payee-info/payee-info.component';
import { SortableTableDirective } from '../../../directives/sortable-table.directive';
import { AccountingDetailsComponent }from '../home/checktransactions/disbursement/accounting-details/accounting-details.component';
import { ToolTipDirective } from '../../../directives/toolTip.directive';
import { ConfirmationService } from './checktransactions/confirmchecks/confirmation.service';
import { DisbursementService } from './checktransactions/disbursement/disbursement.service';

@NgModule({
    declarations: [
      YourTransactionsComponent,
      ReplaceOrVoidChecksComponent,
      DisbursementComponent,
      ViewReplaceOrVoidChecksComponent,
      ControlMessagesComponent,
      VerifyCheckInfoComponent,
      ConfirmchecksComponent,
      PayeeInfoComponent,
      SortableTableDirective,
      AccountingDetailsComponent,
      ToolTipDirective,
    ],
    imports: [
      CommonModule,
      MatTableModule,
      MatSortModule,  
      FormsModule,
      ReactiveFormsModule,
      JsonpModule,
      CdkTableModule,
      MatNativeDateModule,
      RouterModule.forChild(homeRoutes)
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [HomeService, CommondropdownService ,ValidationService,CookieService, DisbursementService,
      UserRolesService, DevdataService,ConfirmationService,
     ],
     exports: [RouterModule, CommonModule,],
  })
  export class HomeModule { }