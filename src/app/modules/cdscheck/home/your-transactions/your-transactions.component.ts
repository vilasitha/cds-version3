import { Component, OnInit, Input, HostListener, EventEmitter, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs/Rx';
import { HomeService } from '../services/home.service';

import { AppComponent } from '../../../../app.component';
import { TransactionsResponse } from './models/all-trasactions';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SharedService } from '../shared/services/shared.service';

import { Sort} from '@angular/material/sort';
import { Output } from '@angular/core';

@Component({
  selector: 'app-your-transactions',
  templateUrl: './your-transactions.component.html',
  styleUrls: ['./your-transactions.component.css'],
  providers:[NGXLogger] 
})
export class YourTransactionsComponent implements OnInit {

  transactionsData: TransactionsResponse[] = [];
  sortedData;

  constructor(private logger: NGXLogger,
    private homeService: HomeService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
  ) {
    this.sortedData = this.transactionsData.slice();  
  }
  ngOnInit() { this.loadYourTransaction();}


  /**
   * This is to get all transaction types
   */
  loadYourTransaction() {
    this.homeService.getCheckTransactions().subscribe(
      (result) => {
        this.transactionsData = result;
        //  let d = JSON.stringify(this.transactionsData);   
      }, (error: any) => {
        this.logger.error("Error in load your transactions method");
        console.error("Error in load your transactions method");
      }
    );
  }


  editOrViewTransactions(clientId: string) {
    let navigationExtras: NavigationExtras = {
      queryParams: { "clientId": clientId }
    };
    for (let checks of this.transactionsData) {
      if (checks.transactionType != null || checks.transactionType != '') {
        if (clientId === checks.clientId && checks.transactionType == 'Replacement') {
          this.sharedService.setTransactionsToDisplay(this.transactionsData);
          this.router.navigate(['home/viewChecks'], navigationExtras);
        }
        else if (clientId === checks.clientId && checks.transactionType === 'Void') {
          this.sharedService.setTransactionsToDisplay(this.transactionsData);
          this.router.navigate(['home/viewChecks'], navigationExtras);
        }
        else if (clientId === checks.clientId && checks.transactionType === 'Disbursement') {
          this.sharedService.setTransactionsToDisplay(this.transactionsData);
          this.router.navigate(['home/viewChecks'], navigationExtras);
        }

      }

    }
  }  //end edit method

  /** sort data table */
  sortData(sort: Sort) {
    const data = this.transactionsData.slice();    
    if (!sort.active || sort.direction == '') {
      this.sortedData = data;
      return;
    }
    this.sortedData = data.sort((item1, item2) => {
      let isAsc = sort.direction == 'asc';
      // console.log("inside sort method");
      // console.log(isAsc);
        switch (sort.active) {        
        case 'transactionType': return compare(item1.transactionType, item2.transactionType, isAsc); 
        case 'checkDate': return compare(+item1.checkDate, +item2.checkDate, isAsc);
        case 'payeeName': return compare(+item1.payeeName, +item2.payeeName, isAsc);
        case 'status': return compare(+item1.status, +item2.status, isAsc);
        default: return 0;
      }
    });
  }
}


function compare(item1, item2, isAsc) {
  return (item1 < item2 ? -1 : 1) * (isAsc ? 1 : -1);
}

