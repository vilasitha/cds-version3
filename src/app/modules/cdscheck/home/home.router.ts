import { Routes, Router } from '@angular/router';

import { HomeComponent } from './home.component';
import { ReplaceOrVoidChecksComponent } from '../home/checktransactions/replace-or-void-checks/replace-or-void-checks.component';
import { VerifyCheckInfoComponent } from '../home/checktransactions/verify-check-info/verify-check-info.component';
import { ConfirmchecksComponent } from '../home/checktransactions/confirmchecks/confirmchecks.component';
import { ViewReplaceOrVoidChecksComponent } from '../home/checktransactions/view-replace-or-void-checks/view-replace-or-void-checks.component';
import { DisbursementComponent } from '../home/checktransactions/disbursement/disbursement.component';
import { YourTransactionsComponent } from './your-transactions/your-transactions.component';
import { PayeeInfoComponent } from '../home/checktransactions/disbursement/payee-info/payee-info.component';
import { AccountingDetailsComponent} from '../home/checktransactions/disbursement/accounting-details/accounting-details.component';
export const homeRoutes: Routes = [

    {
        path: '',
        children: [
            { path: 'yourTransactions', component: YourTransactionsComponent },
            { path: 'voidChecks', component: ReplaceOrVoidChecksComponent },
            { path: 'requestDisbursement', component: DisbursementComponent },
            { path: 'replaceChecks', component: ReplaceOrVoidChecksComponent },
            { path: 'viewChecks', component: ViewReplaceOrVoidChecksComponent },
            { path: 'verifyCheckInfo', component: VerifyCheckInfoComponent },
            { path: 'confirmation', component: ConfirmchecksComponent },
            { path: 'payeeAddress', component: PayeeInfoComponent },
            { path: 'accoutingDetails', component: AccountingDetailsComponent}
        ]
    }

]