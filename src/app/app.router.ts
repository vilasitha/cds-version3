import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/cdscheck/home/home.component';
import { DevdataComponent } from './components/devdata/devdata.component';
import { PageNotFoundComponent } from './modules/cdscheck/home/page-not-found/page-not-found/page-not-found.component';

export const appRoutes: Routes = [

  { path: '', redirectTo: 'devdata', pathMatch: 'full' },
  { path: 'devdata', component: DevdataComponent },
  {
    path: 'home', component: HomeComponent, loadChildren: 'app/modules/cdscheck/home/home.module#HomeModule'
    , data: { preload: true }
  },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: 'not-found' }
];